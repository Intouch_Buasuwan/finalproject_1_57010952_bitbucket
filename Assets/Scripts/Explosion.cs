﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	float ExplosionTimer;

	public int ExplosionType; // 0 = Enemy explosion, 1 = Player explosion mini, 2 = Player explosion finale
	public GameObject ExplosionTypeTwo;
	public GameObject GameOverScreen;

	void Start () {
	
	}

	void Update () {
		ExplosionTimer += Time.deltaTime;
		if (ExplosionType == 0 && ExplosionTimer >= 0.6f) {
			Destroy (this.gameObject);
		} else if (ExplosionType == 1 && ExplosionTimer >= 2) {
			Instantiate (ExplosionTypeTwo, this.transform.position, Quaternion.identity);
			Vector3 pos = this.transform.position;
			pos.x = 0;
			pos.y = 0;
			pos.z = 1;
			Instantiate (GameOverScreen, pos, Quaternion.identity);
			Destroy (this.gameObject);
		} else if (ExplosionType == 2 && ExplosionTimer >= 1.2f) {
			Destroy (this.gameObject);
		}
	}
}
