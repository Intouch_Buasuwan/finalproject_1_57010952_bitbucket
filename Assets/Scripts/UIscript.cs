﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIscript : MonoBehaviour {
	
	public int level;
	public int score;
	public Text TextLives;
	public Text TextLevel;
	public Text TextScore;
	public GameObject DataPlayerShip;

	void Start () {
		score = 0;
	}

	void Update () {
		TextLevel.text = level.ToString ();
		TextScore.text = score.ToString ();

		if (DataPlayerShip != null) {
			Player_script all = DataPlayerShip.GetComponent <Player_script> ();
			TextLives.text = all.PlayerHealth.ToString ();
		}
	}
}
