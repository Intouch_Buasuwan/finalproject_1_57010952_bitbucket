﻿using UnityEngine;
using System.Collections;

public class Power_ups : MonoBehaviour {

	public int PowerUpsType;
	public float PowerUpsSpeed;

	private GameObject UiScore;

	void Start () {
		UiScore = GameObject.FindGameObjectWithTag ("UiData");
	}

	void Update () {
		Vector3 pos = this.transform.position;
		pos.y -= PowerUpsSpeed * Time.deltaTime;
		this.transform.position = pos;
	}

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.tag == "Player") {
			if (PowerUpsType == 0) {
				Player_script upfirerate = other.gameObject.GetComponent <Player_script> ();
				if (upfirerate.PlayerFireRate > 0.3f) {
					upfirerate.PlayerFireRate -= 0.1f;
				} else if (upfirerate.PlayerFireRate > 0.15f) {
					upfirerate.PlayerFireRate -= 0.075f;
				} else if (upfirerate.PlayerFireRate > 0.10f) {
					upfirerate.PlayerFireRate -= 0.025f;
				} else if (upfirerate.PlayerFireRate > 0.05f) {
					upfirerate.PlayerFireRate -= 0.005f;
				} else {
					upfirerate.PlayerFireRate -= 0.001f;
				}
				upfirerate.PlayerUp++;
				Destroy (this.gameObject);
			} else if (PowerUpsType == 1) {
				Player_script uphealth = other.gameObject.GetComponent <Player_script> ();
				uphealth.PlayerHealth++;
				uphealth.PlayerUp++;
				Destroy (this.gameObject);
			} else if (PowerUpsType == 2) {
				UIscript plusscore = UiScore.GetComponent <UIscript> ();
				plusscore.score += 5000;
				Player_script upscore = other.gameObject.GetComponent <Player_script> ();
				upscore.PlayerUp++;
				Destroy (this.gameObject);
			}
		}

		Vector3 pos = this.transform.position;
		pos.z = -1;
		this.transform.position = pos;
	}

	void OnCollisionExit (Collision other) {
		Vector3 pos = this.transform.position;
		pos.z = 0;
		this.transform.position = pos;
	}
}
