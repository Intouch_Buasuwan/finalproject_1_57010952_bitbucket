﻿using UnityEngine;
using System.Collections;

public class Blocks_script : MonoBehaviour {
	
	void Start () {
		
	}

	void Update () {
		
	}

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.tag == "Bullet" ||
			other.gameObject.tag == "PlayerBullet") {
			Destroy (other.gameObject);
			Destroy (this.gameObject);
		} else if (other.gameObject.tag == "Enemy") {
			Destroy (this.gameObject);
		}
	}
}
