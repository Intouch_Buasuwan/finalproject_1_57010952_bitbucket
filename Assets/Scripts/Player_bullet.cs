﻿using UnityEngine;
using System.Collections;

public class Player_bullet : MonoBehaviour {
	
	public float PlayerBulletSpeed;
	public GameObject PlayerShip;

	void Start () {

	}

	void Update () {
		Vector3 pos = this.transform.position;
		pos.y += PlayerBulletSpeed * Time.deltaTime;
		this.transform.position = pos;
	}

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.tag == "Enemy") {
			Enemy_script n = other.gameObject.GetComponent <Enemy_script> ();
			n.EnemyHealth--;
			Destroy (this.gameObject);
		}
	}
}
