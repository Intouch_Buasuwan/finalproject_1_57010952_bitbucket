﻿using UnityEngine;
using System.Collections;

public class Enemy_script : MonoBehaviour {

	int TempEnemyHealth;
	bool EnemyMoveLeft;
	float EnemyTimer;
	float EnemyDistance;

//	public int EnemyScore;
	public int EnemyGift;
	public int EnemyHealth;
	public float EnemySpeed;
	public GameObject EnemyBullet;
	public GameObject EnemyExplosion;
	public GameObject PowerUp1;
	public GameObject PowerUp2;
	public GameObject PowerUp3;
	public AudioClip SoundEnemyHit;

	private AudioSource source;
	private GameObject DataUI;
	private GameObject LevelSpawner;

	void Awake () {
		DataUI = GameObject.FindGameObjectWithTag ("UiData");
		LevelSpawner = GameObject.FindGameObjectWithTag ("LevelSpawner");
	}

	void Start () {
		TempEnemyHealth = EnemyHealth;
		EnemyMoveLeft = false;
		EnemyDistance = 4;
		source = GetComponent <AudioSource> ();
	}

	void Update () {
//		EnemyScore = 0;

		EnemyTimer += Time.deltaTime;

		if (TempEnemyHealth > EnemyHealth) {
			source.PlayOneShot (SoundEnemyHit, 1);
			TempEnemyHealth = EnemyHealth;
//			EnemyScore += 100;

			UIscript hitscore = DataUI.GetComponent <UIscript> ();
			hitscore.score += 100;
		}

		if (EnemyHealth <= 0) {
//			EnemyScore += 400;
			UIscript hitscore = DataUI.GetComponent <UIscript> ();
			hitscore.score += 400;
			Level_script deathcount = LevelSpawner.GetComponent <Level_script> ();
			deathcount.EnemyDeathCount++;
			Instantiate (EnemyExplosion, this.transform.position, Quaternion.identity);
			if (EnemyGift == 1) {
				int chance = Random.Range (0, 25);
				Vector3 pwu = this.transform.position;
				pwu.z = -0.5f;
				if (chance == 0) {
					Instantiate (PowerUp1, pwu, Quaternion.identity);
				} else if (chance == 1) {
					Instantiate (PowerUp2, pwu, Quaternion.identity);
				} else if (chance == 2) {
					Instantiate (PowerUp3, pwu, Quaternion.identity);
				}
			}
			Destroy (this.gameObject);
		}

		if (EnemyTimer >= 1) {
			int chance = Random.Range (0, 9);
			if (chance == 0) {
				Vector3 shoot = this.transform.position;
				shoot.y -= 0.475f;
				Instantiate (EnemyBullet, shoot, Quaternion.identity);
			}
			EnemyTimer = 0;
		}

		Vector3 pos = this.transform.position;
		if (EnemyMoveLeft == false) {
			pos.x += EnemySpeed * Time.deltaTime;
			EnemyDistance += EnemySpeed * Time.deltaTime;
			if (EnemyDistance >= 8) {
				EnemyMoveLeft = true;
				EnemyDistance = 0;
				pos.y -= 1;
			}
			this.transform.position = pos;
		} else if (EnemyMoveLeft == true) {
			pos.x -= EnemySpeed * Time.deltaTime;
			EnemyDistance += EnemySpeed * Time.deltaTime;
			if (EnemyDistance >= 8) {
				EnemyMoveLeft = false;
				EnemyDistance = 0;
				pos.y -= 1;
			}
			this.transform.position = pos;
		}
	}

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.tag == "Player") {
			Player_script Player = other.gameObject.GetComponent <Player_script> ();
			Player.PlayerHealth = 0;
		}
	}
}
