﻿using UnityEngine;
using System.Collections;

public class Border_endgame : MonoBehaviour {

	private GameObject AlienReachPlayer;
	
	void Start () {
		AlienReachPlayer = GameObject.FindGameObjectWithTag ("Player");
	}

	void Update () {
	
	}

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.tag == "Enemy") {
			Player_script end = AlienReachPlayer.GetComponent <Player_script> ();
			end.PlayerHealth = 0;
		}
	}
}
