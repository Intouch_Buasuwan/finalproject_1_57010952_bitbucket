﻿using UnityEngine;
using System.Collections;

public class Level_script : MonoBehaviour {

	int NumLevel;
	int EnemyRow;
	int EnemyColumn;
	float SpawnPowerTimer;

	public int EnemySpawnCount;
	public int EnemyDeathCount;
	public GameObject Power1;
	public GameObject Power2;
	public GameObject UiLevel;
	public GameObject[] Enemies;
	GameObject[] DataEnemies;

	void Awake () {
		UIscript nextwave = UiLevel.GetComponent <UIscript> ();
		nextwave.level++;
		NumLevel++;
		while(EnemyColumn < Enemies.Length) {
			while(EnemyRow < 9) {
				Vector3 pos = this.transform.position;
				pos.x += EnemyRow;
				pos.y -= EnemyColumn;
				Instantiate (Enemies[EnemyColumn], pos, Quaternion.identity);
				EnemyRow++;
				EnemySpawnCount++;
			}
			EnemyColumn++;
			EnemyRow = 0;
		}

		DataEnemies = GameObject.FindGameObjectsWithTag ("Enemy");
		for (int n = EnemyDeathCount; n < EnemySpawnCount; n++) {
			Enemy_script temp = DataEnemies[n].GetComponent <Enemy_script> ();
			temp.EnemyHealth = NumLevel;
		}
		DataEnemies = null;

		EnemyColumn = 0;
		EnemyRow = 0;
	}

	void Update () {
		SpawnPowerTimer += Time.deltaTime;
		if (SpawnPowerTimer >= 20) {
			Vector3 pos = this.transform.position;
			int posx = Random.Range (-7, 7);
			pos.x = posx;
			pos.z = -0.5f;
			int n = Random.Range (0, 5);
			if (n == 0 || n == 1 || n == 2) {
				Instantiate (Power1, pos, Quaternion.identity);
			} else {
				Instantiate (Power2, pos, Quaternion.identity);
			}
			SpawnPowerTimer = 0;
		}

		if(EnemyDeathCount >= EnemySpawnCount) {
			UIscript nextwave = UiLevel.GetComponent <UIscript> ();
			nextwave.level++;
			NumLevel++;
			while(EnemyColumn < Enemies.Length) {
				while(EnemyRow < 9) {
					Vector3 pos = this.transform.position;
					pos.x += EnemyRow;
					pos.y -= EnemyColumn;
					Instantiate (Enemies[EnemyColumn], pos, Quaternion.identity);
					EnemyRow++;
					EnemySpawnCount++;
				}
				EnemyColumn++;
				EnemyRow = 0;
			}

			DataEnemies = GameObject.FindGameObjectsWithTag ("Enemy");
			for (int n = 0; n < EnemySpawnCount - EnemyDeathCount; n++) {
				Enemy_script temp = DataEnemies[n].GetComponent <Enemy_script> ();
				temp.EnemyHealth = NumLevel;
			}
			DataEnemies = null;
		}
		EnemyColumn = 0;
		EnemyRow = 0;
	}
}
