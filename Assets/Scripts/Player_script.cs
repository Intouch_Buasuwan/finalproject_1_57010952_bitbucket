﻿using UnityEngine;
using System.Collections;

public class Player_script : MonoBehaviour {
	
	int bulletamount;
	int TempPlayerHealth;
	float ShotCooldown;

//	public int PlayerScore;
	public int PlayerUp;
	public int PlayerHealth;
	public float PlayerTime;
	public float PlayerSpeed;
	public float PlayerFireRate;
	public GameObject PlayerBullet;
	public GameObject PlayerDead;
	public AudioClip SoundPlayerDamage;
	public AudioClip SoundPlayerPowerUps;
	
	private AudioSource source;
	private GameObject DataUI;
//	private GameObject[] DataEnemies;

	void Awake () {
		DataUI = GameObject.FindGameObjectWithTag ("UiData");
	}

	void Start () {
		bulletamount = 0;
		source = GetComponent <AudioSource> ();
		TempPlayerHealth = PlayerHealth;
		PlayerUp = 0;
//		DataEnemies = GameObject.FindGameObjectsWithTag ("Enemy");
	}

	void Update () {
//		Rigidbody rgd = this.gameObject.GetComponent<Rigidbody> ();
//		Vector3 velo = rgd.velocity;
//		rgd.velocity = velo;

//		for (int x = 0; x < DataEnemies.Length; x++) {
//			if (DataEnemies[x] == null) {
//				continue;
//			}
//			Enemy_script temp = DataEnemies[x].GetComponent <Enemy_script> ();
//			PlayerScore += temp.EnemyScore;
//		}

		PlayerTime += Time.deltaTime;
		if (PlayerTime > 0.1f) {
			UIscript timescore = DataUI.GetComponent <UIscript> ();
			timescore.score += 10;
			PlayerTime = 0;
		}

		if (PlayerUp >= 1) {
			source.PlayOneShot (SoundPlayerPowerUps, 0.5f);
			PlayerUp = 0;
		}

		if (TempPlayerHealth > PlayerHealth) {
			source.PlayOneShot (SoundPlayerDamage, 1);
			TempPlayerHealth = PlayerHealth;
		}

		if (PlayerHealth <= 0) {
			Instantiate (PlayerDead, this.transform.position, Quaternion.identity);
			Destroy (this.gameObject);
		}

		ShotCooldown += Time.deltaTime;
		if (ShotCooldown >= PlayerFireRate) {
			if (Input.GetKey (KeyCode.Space)) {
				Vector3 pos = this.transform.position;
				if(bulletamount == 0) {
					pos.x -= 0.135f;
					pos.y += 0.44f;
					bulletamount++;
				} else {
					pos.x += 0.135f;
					pos.y += 0.44f;
					bulletamount--;
				}
				Instantiate (PlayerBullet, pos, Quaternion.identity);
				ShotCooldown = 0;
			}
		}

		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) {
//			rgd = this.gameObject.GetComponent<Rigidbody> ();
//			rgd.AddForce (-Speed * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
			Vector3 pos = this.transform.position;
			pos.x -= PlayerSpeed * Time.deltaTime;
			this.transform.position = pos;
		} else if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) {
//			rgd = this.gameObject.GetComponent<Rigidbody> ();
//			rgd.AddForce (Speed * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
			Vector3 pos = this.transform.position;
			pos.x += PlayerSpeed * Time.deltaTime;
			this.transform.position = pos;
		}

		Vector3 checkBorder = this.transform.position;

		if (checkBorder.x > 8.4f) {
			checkBorder.x = 8.3f;
		} else if (checkBorder.x < -8.4f) {
			checkBorder.x = -8.3f;
		}
	}
}
