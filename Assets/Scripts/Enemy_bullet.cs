﻿using UnityEngine;
using System.Collections;

public class Enemy_bullet : MonoBehaviour {

	public float EnemyBulletSpeed;

	void Start () {
	
	}

	void Update () {
		Vector3 pos = this.transform.position;
		pos.y -= EnemyBulletSpeed * Time.deltaTime;
		this.transform.position = pos;
	}

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.tag == "Player") {
			Player_script n = other.gameObject.GetComponent <Player_script> ();
			n.PlayerHealth--;
			Destroy (this.gameObject);
		} else if (other.gameObject.tag == "Enemy") {
			Vector3 pos = this.transform.position;
			pos.z = 0.25f;
			this.transform.position = pos;
		}
	}

	void OnCollisionExit (Collision other) {
		if (other.gameObject.tag == "Enemy") {
			Vector3 pos = this.transform.position;
			pos.z = 0;
			this.transform.position = pos;
		}
	}
}
